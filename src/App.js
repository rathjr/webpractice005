import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Mycard from './components/Mycard';
import { Container,Row } from 'react-bootstrap';
import Mytable from './components/Mytable';

export default class App extends React.Component {
   
    constructor(props){
        super(props);
        this.state={
            foods:[
                {
                    id:1,
                    name: "Pizza",
                    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTxh-YtNAClJ0rt4tuYWVkrg8BmlA_lMU1Tjw&usqp=CAU",
                    qty:0,
                    price:10,
                    total:0,
                },
                {
                    id:2,
                    name: "Kfc",
                    image: "https://www.joc.com/sites/default/files/field_feature_image/KFC_0.png",
                    qty:0,
                    price:25,
                    total:0,
                },
                {
                    id:3,
                    name: "Ice cream",
                    image: "https://static.independent.co.uk/2021/03/31/14/newFile-6.jpg?width=982&height=726&auto=webp&quality=75",
                    qty:0,
                    price:50,
                    total:0,
                },
                {
                    id:4,
                    name: "Bread",
                    image: "https://images.immediate.co.uk/production/volatile/sites/30/2020/08/easy-bread-7455dce.jpg?quality=90&webp=true&resize=440,400",
                    qty:0,
                    price:40,
                    total:0,
                },
            ],
        }
    }

    onAdd=(index)=>{
        let tmp=[...this.state.foods]
        tmp[index].qty++
        tmp[index].total=tmp[index].qty*tmp[index].price
        this.setState({
            foods:tmp
        })
       
    }
    onDel=(index)=>{
        let tmp=[...this.state.foods]
        tmp[index].qty--
        tmp[index].total=tmp[index].qty*tmp[index].price
        this.setState({
            foods:tmp
        })
       
    }
    onClear=()=>{
        console.log("clear")
        let tmp=[...this.state.foods]
        tmp.map(item=>{
            item.qty=0
            item.total=0
        })
        this.setState({
            foods:tmp
        }) 
    }
        
    render(){
        return (
           <Container>
               <Row>
                   <Mycard foods={this.state.foods} onAdd={this.onAdd} onDel={this.onDel}/>
               </Row>
               <Row>
           <Mytable foods={this.state.foods}  onClear={this.onClear} />
       </Row>
           </Container>
           
        )
    }
   

    }