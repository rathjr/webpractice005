import React from 'react'
import {Table,Button} from 'react-bootstrap'

export default function Mytable({foods,onClear}) {

    console.log(foods);

    let temp=foods.filter(item=>{
        return item.qty>0
    })
    return (
        <>
        <Button onClick={onClear }  style={{width:"80px",margin:"15px 10px 10px 0px"}} variant="danger">Clear</Button>
        <Button style={{width:"80px",margin:"15px 10px 10px 0px"}} variant="warning">{temp.length} food</Button>
        <Table striped bordered hover>
  <thead>
    <tr>
      <th>#</th>
      <th>Name</th>
      <th>Qty</th>
      <th>Price </th>
      <th>Total</th>
    </tr>
  </thead>
  <tbody>
      {temp.map((item, index)=>(
    <tr>
        
      <td>{index+1}</td>
      <td>{item.name}</td>
      <td>{item.qty}</td>
      <td>{item.price}</td>
      <td>{item.total}</td>
    </tr>
   ))}
  </tbody>
</Table>
        </>
    );
}
