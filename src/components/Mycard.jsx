import React, { Component } from 'react'
import { Card, Button,Col} from 'react-bootstrap'

class Mycard extends Component {


    render() {
        console.log(this.props.foods)
        return (
            <>{
                this.props.foods.map((item, index) => (
                    <Col xs="3">
                    <Card key={index}>
                        <Card.Img variant="top" src={item.image} style={{height:"200px"}} />
                        <Card.Body>
                            <Card.Title>{item.name}</Card.Title>
                            <Card.Text>
                                {item.price} $
</Card.Text>
                            <Button disabled={item.qty===0} onClick={()=>this.props.onDel(index)}variant="danger">-</Button>{' '}
                            <Button onClick={()=>this.props.onAdd(index)} variant="primary">+</Button>{' '}
                            <Button variant="warning">{item.qty}</Button>{' '}
                            <h3 style={{marginTop:"8px"}}>Total : {item.total} $</h3>
                        </Card.Body>
                    </Card>
                     </Col>
                    
                    ))
            }</>
        );
    }
}
export default Mycard;